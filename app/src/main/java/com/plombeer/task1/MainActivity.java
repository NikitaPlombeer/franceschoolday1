package com.plombeer.task1;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    public EditText editText;
    public String sign;
    public Double first;
    public boolean secondTapOnEq;

    public void goToTwo(View v) {
        Intent intent = new Intent(this, Main2Activity.class);
        intent.putExtra("sex", "Жора красавчик");
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.editText = (EditText) findViewById(R.id.editText);
        this.secondTapOnEq = false;
    }

    public void onEqClick(View v) {
        String text = this.editText.getText().toString();
        if(!text.trim().equals("")) {
            double second = Double.parseDouble(text);
            if (!this.secondTapOnEq) {
                this.first = second;
            }
            switch (this.sign) {
                case "+":
                    this.editText.setText(String.valueOf(this.first + second));
                    break;
                case "-":
                    this.editText.setText(String.valueOf(this.first - second));
                    break;
                case "*":
                    this.editText.setText(String.valueOf(this.first * second));
                    break;
                case "/":
                    this.editText.setText(String.valueOf(this.first / second));
                    break;
            }
            this.secondTapOnEq = true;
        }
    }

    public void onNumClick(View v) {
        Button button = (Button) v;
        String tmp = button.getText().toString();
        this.editText.setText(this.editText.getText().toString() + tmp);
        this.secondTapOnEq = false;
    }

    public void onSignClick(View v) {
        Button button = (Button) v;
        if (!this.editText.getText().toString().trim().equals("")) {
            this.first = Double.parseDouble(this.editText.getText().toString());
        } else {
            this.first = 0.;
        }
        this.sign = button.getText().toString();
        this.editText.setText("");
        this.secondTapOnEq = false;
    }

    public void onClrClick(View v) {
        if (this.editText.getText().toString().trim().equals("")){
            this.sign = "";
            this.first = null;
            this.secondTapOnEq = false;
        } else {
            this.editText.setText("");
        }

    }
}
